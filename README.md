# pico-pytest

Smallest possible implementation of pytest core functionality.

Implements:

* recursive test collection from `cwd` down
* test execution
* simple reporting if a test failed or not

try it out - clone this repo and cd into the clone (example are for posix systems):

```console
# create venv
$ python3.6 -m venv pico-pytest

# activate it
$ source pico-pytest/bin/activate
# or on windows: pico-pytest/Scripts/activate.bat

# install pico-pytest in development mode
$ pip install -e .

# enter the directory with the test demo "suite"
$ cd demo

# run pico-pytest
$ pico-pytest
```

output:

```console

================================================================================ 
 F...F...F... 
 ================================================================================
0 tests failed :(
<module 'test_module_1' from '[...]pico-pytest/demo/test_module_1.py'>::test_1_1
<module 'test_module_2' from '[...]pico-pytest/demo/test_module_2.py'>::test_2_1
<module 'test_module_3' from '[...]pico-pytest/demo/nested/test_module_3.py'>::test_3_1
```
