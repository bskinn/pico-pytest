from setuptools import setup, find_packages

setup(
    name="pico-pytest",
    packages=find_packages(),
    entry_points={'console_scripts': [
        'pico-pytest = pico_pytest.pytest:main',
    ]},
)
